ARG BASE_TAG=8-jre-slim
FROM openjdk:${BASE_TAG}

LABEL maintainer="k3rnel-pan1c" version="1.0"

ARG U_ID=1010
ARG G_ID=1010
# pssible timecodes https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
ARG TZ=Etc/GMT-1
ENV TZ=${TZ}

# creates homefolder & user for the server and sets the TimeZone to use for log messages
RUN mkdir -p /home/mcserver \
  && chown ${U_ID}:${G_ID} /home/mcserver \
  && groupadd -g ${G_ID} mcserver \
  && useradd -d "/home/mcserver" -u ${U_ID} -g ${G_ID} -m -s /bin/bash mcserver \
  && ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime && echo ${TZ} > /etc/timezone

ARG MOD_PACK_NAME
ARG PACK_VERSION
ARG DL_URL_PATTERN="https://minecraft.curseforge.com/projects/${MOD_PACK_NAME}/files/${PACK_VERSION}/download"

ENV MOD_PACK_NAME=${MOD_PACK_NAME} \
  PACK_VERSION=${PACK_VERSION}

RUN apt-get update && apt-get install --no-install-recommends -y curl && rm -rf /var/lib/apt/lists/* \
  && echo "Downloading Modpack ..."\
  && curl -fsSL "${DL_URL_PATTERN}" -o /home/mcserver/server.zip \
  && echo "Finished download! Continuing setup ..."\
  && unzip -q /home/mcserver/server.zip -d /home/mcserver/ \
  && rm /home/mcserver/server.zip \
  && mkdir /home/mcserver/world /home/mcserver/logs \
  && chown -R ${U_ID}:${G_ID} /home/mcserver \
  # but of course, we all already read the EULA ;-)
  && echo "eula=true" >> /home/mcserver/eula.txt 

# coppy additional mods and configs
COPY --chown=mcserver:mcserver ./more-mods /home/mcserver/mods
COPY --chown=mcserver:mcserver ./more-cfgs /home/mcserver/config

# finishing touches
# only force voumes for world
EXPOSE 25565
VOLUME [ "/home/mcserver/world", "/home/mcserver/logs" ]
COPY ./server-scripts /
ENTRYPOINT [ "/docker-entrypoint.sh" ]
